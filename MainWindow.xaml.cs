﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfApp2.Model;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //https://www.cnblogs.com/lindexi/p/9109040.html
        RectangleElement re;
        public MainWindow()
        {
            InitializeComponent();
            re = new RectangleElement();
            //Content = re;  //开关drawcontext或canvas
        }

        private void Re_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {

        }

        //public class RectangleElement2 : UIElement
        //{

        //    protected override void OnRender(DrawingContext drawingContext)
        //    {
        //        drawingContext.DrawRectangle(Brushes.Red, null, new Rect(0, 0, 100, 20));
        //        base.OnRender(drawingContext);
        //    }
        //}
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //var bitmapImage = new BitmapImage(new Uri("pack://application:,,,/1.jpg"));
            //var drawingVisual = new DrawingVisual();
            //using (DrawingContext dc = drawingVisual.RenderOpen())
            //{
            //    dc.DrawImage(bitmapImage, new Rect(100, 100, 50, 50));
            //}

            //Element.ContainerVisual.Children.Add(drawingVisual);
            // stt();

            var Name = "123";
            var results2 = $"Hello {Name}"; //$拼接
            var results = $"Hello {Name}{new Program()?.GetCnblogsSite()}";//{}可以直接插入代码

            var qq = $"{results2}";

            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(10);
                    Dispatcher.BeginInvoke(new Action(() =>
                    {
                        re.InvalidateVisual();
                    }));
                }
            });
            //var p = new Program()?.GetCnblogsSite();

            //DrawingVisual dv=new DrawingVisual();
            //var drawingContext = dv.RenderOpen();
            //drawingContext.DrawRectangle(Brushes.Red, null, new Rect(0, 0, this.ActualWidth, ActualHeight));
            ////drawingContext.Close();
        }


        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            //new Task().Run(() => {

            //        this.Dispatcher.BeginInvoke(new Action(() =>
            //         {
            //         }));
            re.score += 10;
            re.score1 += 10;
            re.score2 += 10;
            re.score3 += 10;
            re.score4 += 10;
            //    }).Start();
            if (e.Key == Key.Space)
            {
                Task.Run(() =>
                {
                    for (int i = 0; i < 300; i++)
                    {
                        re.y = i;

                        Thread.Sleep(10);
                        Dispatcher.BeginInvoke(new Action(() =>
                         {
                             re.score += 10;
                             re.InvalidateVisual();
                         }));
                    }
                });
            }
            if (e.Key == Key.Up)
            {
                re.y--;
            }
            if (e.Key == Key.Down)
            {
                re.y++;
            }
            if (e.Key == Key.Left)
            {
                re.x--;
            }
            if (e.Key == Key.Right)
            {
                re.x++;
            }
            //re.btn.InvalidateVisual();
            re.InvalidateVisual();

        }
        private void drawText(double x, double y, string text, Color color, Canvas canvasObj)
        {

            TextBlock textBlock = new TextBlock();

            textBlock.Text = text;

            textBlock.Foreground = new SolidColorBrush(color);

            Canvas.SetLeft(textBlock, x);

            Canvas.SetTop(textBlock, y);

            canvasObj.Children.Add(textBlock);

        }
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            DispatcherTimer timer = new DispatcherTimer();
            //timer.Interval = new TimeSpan(0, 0, 1);//设置的间隔为一分钟
            timer.Interval = TimeSpan.FromMilliseconds(50);
            timer.Tick += Timer_Tick;
            timer.IsEnabled = true;
            timer.Start();
        }
        int y = 0;
        int x = 10;
        bool isadd = false;
        private void Timer_Tick(object? sender, EventArgs e)
        {
            int dd = 0;
            try
            {
                foreach (var item in canvas.Children)
                {
                    if (item is TextBlock)
                    {
                        dd += int.Parse((item as TextBlock).Text);
                        if (isadd)
                        {
                            dd += 10;
                        }
                        break;
                    }
                }
            }
            catch (Exception)
            {
                dd = 0;
            }

            //清除canvas画布
            canvas.Children.Clear();

            //直线对象
            Line mydrawline = new Line();
            mydrawline.Stroke = Brushes.Black;//外宽颜色，在直线里为线颜色
                                              //mydrawline.Stroke = new SolidColorBrush(Color.FromArgb(0xFF, 0x5B, 0x9B, 0xD5));//自定义颜色则用这句
            mydrawline.StrokeThickness = 20;//线宽度
            mydrawline.Height = y;
            mydrawline.Width = x;
            mydrawline.X1 = 10;
            mydrawline.Y1 = 0;
            mydrawline.X2 = x;
            mydrawline.Y2 = y;
            y++;
            if (y >= Height)
            {
                x++;
            }
            //将直线对象添加到canvas画布中
            canvas.Children.Add(mydrawline);




            //for (int i = 0; i < 5; i++)
            //{

            Rectangle rectangle2 = new Rectangle         // 准备一个方块
            {
                Width = 50,
                Height = 50,
                Fill = Brushes.LightGray
            };
            Canvas.SetLeft(rectangle2, new Random().Next(0, (int)Width));
            Canvas.SetTop(rectangle2, new Random().Next(0, (int)Height));

            this.canvas.Children.Add(rectangle2);      // 在指定地方放入方块
                                                       //Thread.Sleep(1000);
                                                       //}
            Rectangle rectangle = new Rectangle         // 准备一个方块
            {
                Width = 50,
                Height = 50,
                Fill = Brushes.LightGray
            };
            Canvas.SetLeft(rectangle, mx);
            Canvas.SetTop(rectangle, my);

            this.canvas.Children.Add(rectangle);      // 在指定地方放入方块



            drawText(200, 200, text: fpscount().ToString(), Color.FromRgb(200, 0, 0), canvas);
        }
        public static string GetTimeStamp()
        {
            TimeSpan ts = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString();
        }

        int lastTime = int.Parse(GetTimeStamp());// ms
        int fps = 0;
        int frameCount = 0;

        int fpscount()
        {

            ++frameCount;

            int curTime = int.Parse(GetTimeStamp());// ms
            if (curTime - lastTime > 1) // 取固定时间间隔为1秒
            {
                fps = frameCount;
                frameCount = 0;
                lastTime = curTime;
            }
            return fps;
        }
        int mx = 0;
        int my = 0;

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            Point pp = Mouse.GetPosition(this);//WPF方法

            re.x = (int)pp.X;
            re.y = (int)pp.Y;

            if ((int)pp.X > 0 && (int)pp.Y > 0)
            {
                mx = (int)pp.X;
                my = (int)pp.Y;
            }

            Title = mx + "," + my;

        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void Window_MouseEnter(object sender, MouseEventArgs e)
        {
            isadd = true;
        }

        private void Window_MouseLeave(object sender, MouseEventArgs e)
        {
            isadd = false;

        }
    }
    //protected override void OnRender(DrawingContext drawingContext)
    //{
    //    var pen = new Pen(Brushes.Black, 1);
    //    Rect rect = new Rect(20, 20, 30, 60);
    //    drawingContext.DrawRectangle(null, pen, rect);
    //}
    //protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
    //{
    //    var drawingContext = dv.RenderOpen();
    //    drawingContext.DrawRectangle(Brushes.Red, null, new Rect(0, 0, this.ActualWidth, ActualHeight));
    //    drawingContext.Close();
    //    base.OnRenderSizeChanged(sizeInfo);
    //}

    //var pen = new Pen(Brushes.Black, 1);
    //Rect rect = new Rect(20, 20, 30, 60);
    //drawingContext.DrawRectangle(null, pen, rect);
    internal class Program
    {
        public Program()
        {
        }
        internal object GetCnblogsSite2()
        {
            return "test";
        }
        internal object GetCnblogsSite()
        {
            return "test";
        }
    }
}
