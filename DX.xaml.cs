﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using D2D = SharpDX.Direct2D1;
using WIC = SharpDX.WIC;
using DW = SharpDX.DirectWrite;
using DXGI = SharpDX.DXGI;
using System.Windows.Interop;
using SharpDX;
using SharpDX.Mathematics.Interop;
using SharpDX.Direct2D1;
using SharpDX.DXGI;
using AlphaMode = SharpDX.Direct2D1.AlphaMode;

namespace WpfApp2
{
    /// <summary>
    /// DX.xaml 的交互逻辑
    /// </summary>
    public partial class DX : Window
    {
        public DX()
        {
            InitializeComponent();
            CompositionTarget.Rendering += OnRendering; ;

        }

        private D2D.SolidColorBrush _redBrush;

        private D2D.SolidColorBrush _greenBrush;

        private D2D.SolidColorBrush _blueBrush;
        private float _dx = 1;
        private float _dy = 1;
        private float _x;
        private float _y;

        private Random ran = new Random();
        private void OnRendering(object? sender, EventArgs e)
        {
            //https://vimsky.com/zh-tw/examples/detail/csharp-ex-SharpDX.Direct2D1-RenderTarget-Clear-method.html
            //https://www.cnblogs.com/lindexi/p/12086925.html
            if (_renderTarget == null)
            {
                return;
            }

            var ellipse = new D2D.Ellipse(new RawVector2(new Random().Next(0, (int)Width), new Random().Next(0, (int)Height)), 10, 10);
            var ellipse2 = new D2D.Ellipse(new RawVector2(new Random().Next(0, (int)Width), new Random().Next(0, (int)Height)), 10, 10);

            var brush = new D2D.SolidColorBrush(_renderTarget, new RawColor4(1, 0, 0, 1));
            _renderTarget.BeginDraw();
            _renderTarget.Clear(new RawColor4(0.0f, 0.0f, 0.0f, 0.0f));
            _renderTarget.DrawEllipse(ellipse, brush, 1);
            _renderTarget.DrawEllipse(ellipse2, brush, 1);
            _renderTarget.EndDraw();


            //D2D.SolidColorBrush brush = null;

            //switch (ran.Next(3))
            //{
            //    case 0:
            //        brush = _redBrush;
            //        break;
            //    case 1:
            //        brush = _greenBrush;
            //        break;
            //    case 2:
            //        brush = _blueBrush;
            //        break;
            //}

            //_renderTarget.BeginDraw();
            //_renderTarget.DrawRectangle(new D2D.RoundedRectangle(_x, _y, _x + 10, _y + 10), brush, 1);
            //_renderTarget.EndDraw();

            //_x = _x + _dx;
            //_y = _y + _dy;
            //if (_x >= ActualWidth - 100 || _x <= 0)
            //{
            //    _dx = -_dx;
            //}

            //if (_y >= ActualHeight - 100 || _y <= 0)
            //{
            //    _dy = -_dy;
            //}
        }

        private D2D.RenderTarget _renderTarget;
        RawColor4 ColorToRaw4(Color color)
        {

            const float n = 255f;
            return new RawColor4(color.R / n, color.G / n, color.B / n, color.A / n);
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var factory = new D2D.Factory();

            var pixelFormat = new D2D.PixelFormat(Format.Unknown, AlphaMode.Unknown);

            var hwndRenderTargetProperties = new D2D.HwndRenderTargetProperties();
            hwndRenderTargetProperties.Hwnd = new WindowInteropHelper(this).Handle;
            hwndRenderTargetProperties.Hwnd = new WindowInteropHelper(this).Handle;
            hwndRenderTargetProperties.PixelSize = new Size2((int)ActualWidth, (int)ActualHeight);

            var renderTargetProperties = new D2D.RenderTargetProperties(D2D.RenderTargetType.Default, pixelFormat,
                96, 96, D2D.RenderTargetUsage.None, D2D.FeatureLevel.Level_DEFAULT);

            _renderTarget = new D2D.WindowRenderTarget(factory, renderTargetProperties, hwndRenderTargetProperties);


        }
    }
}
